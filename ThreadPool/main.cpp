#include <iostream>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include "thread_safe_queue.hpp"

using namespace std;

typedef boost::function<void()> Task;

class ThreadPool : boost::noncopyable
{
public:
    ThreadPool(size_t no_of_threads) : no_of_threads_(no_of_threads)
    {
        for(size_t i = 0; i < no_of_threads_; ++i)
            threads_.create_thread(boost::bind(&ThreadPool::run, this));
    }

    ~ThreadPool()
    {
        for(size_t i = 0; i < no_of_threads_; ++i)
            task_queue_.push(END_OF_WORK);

        threads_.join_all();
    }

//    void enqueue(Task task)
//    {
//        if (!task)
//            throw std::runtime_error("Task can't be empty...");

//        task_queue_.push(task);
//    }

    template <typename T, typename F>
    boost::unique_future<T> enqueue(F fun)
    {
        auto task = boost::make_shared<boost::packaged_task<T>>(fun);
        boost::unique_future<T> fresult = task->get_future();
        task_queue_.push([task]() { (*task)(); });

        return fresult;
    }
private:
    static const Task END_OF_WORK;

    size_t no_of_threads_;
    ThreadSafeQueue<Task> task_queue_;
    boost::thread_group threads_;

    void run()
    {
        while (true)
        {
            Task task;
            task_queue_.wait_and_pop(task);

            if (!task)
                return;

            task();
        }
    }
};

const Task ThreadPool::END_OF_WORK;

void do_something(const string& data)
{
    cout << "Start: " << data << endl;
    boost::this_thread::sleep_for(boost::chrono::milliseconds(2000));
    cout << "Done: " << data << endl;
}

int calc_something(int data)
{
    cout << "Start: " << data << endl;
    boost::this_thread::sleep_for(boost::chrono::milliseconds(2000));
    return data * data;
}

int main()
{
    ThreadPool thread_pool(10);

    for(int i = 0; i < 10; ++i)
    {
        thread_pool.enqueue<void>(boost::bind(&do_something, i));
    }

    vector<boost::unique_future<int>> results;

    for(int i = 0; i < 10; ++i)
        results.push_back(
                    thread_pool.enqueue<int>(boost::bind(&calc_something, i)));

    cout << "Results: ";

    for(auto& r : results)
    {
        cout << r.get() << endl;
    }

    cout << endl;
}
