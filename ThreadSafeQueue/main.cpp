#include <iostream>
#include "thread_safe_queue.hpp"

using namespace std;

typedef string DataType;

class ProducerConsumer
{
    ThreadSafeQueue<DataType> queue_;
public:
    void produce(const DataType& data, int consumer_count)
    {
        DataType item = data;
        while (next_permutation(item.begin(), item.end()))
        {
            cout << "Producing: " << item << endl;
            queue_.push(item);
            boost::this_thread::sleep_for(boost::chrono::milliseconds(200));
        }

        for(int i = 0; i < consumer_count; ++i)
            queue_.push("");
    }

    void consumer(int id)
    {
        while (true)
        {
            DataType data;
            queue_.wait_and_pop(data);

            if (data == "")
            {
                cout << "Consumer#" << id << " ends job..." << endl;
                return;
            }

            process_data(id, data);
        }
    }
private:
    void process_data(int id, const DataType& data)
    {
        cout << "Consumer#" << id << " is processing: " << data << endl;
        boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
    }
};

int main()
{
    ProducerConsumer pc;

    boost::thread thd1(&ProducerConsumer::produce, &pc, "abcd", 2);
    boost::thread thd2(&ProducerConsumer::consumer, &pc, 1);
    boost::thread thd3(&ProducerConsumer::consumer, &pc, 2);

    thd1.join();
    thd2.join();
    thd3.join();
}
