#ifndef THREAD_SAFE_QUEUE_HPP
#define THREAD_SAFE_QUEUE_HPP

#include <queue>
#include <boost/thread.hpp>

template <typename T>
class ThreadSafeQueue
{
    std::queue<T> queue_;
    boost::mutex mtx_;
    boost::condition_variable cond_;
public:
    ThreadSafeQueue()
    {}

    void push(const T& item)
    {
        boost::lock_guard<boost::mutex> lk(mtx_);
        queue_.push(item);
        cond_.notify_one();
    }

    void wait_and_pop(T& item)
    {
        boost::unique_lock<boost::mutex> lk(mtx_);
        cond_.wait(lk, [&] { return !queue_.empty(); });
        item = queue_.front();
        queue_.pop();
    }

    bool try_pop(T& item)
    {
        boost::lock_guard<boost::mutex> lk(mtx_);

        if (queue_.empty())
            return false;

        item = queue_.front();
        queue_.pop();

        return true;
    }
};

#endif // THREAD_SAFE_QUEUE_HPP
