#include <iostream>
#include <string>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/bind.hpp>

using namespace std;

void f(string name)
{
    cout << "Start " << name << endl;
    boost::this_thread::sleep_for(boost::chrono::milliseconds(2000));
    cout << "End " << name << endl;
}

int calculate_square(int x)
{
    int interval = rand() % 5000;

    boost::this_thread::sleep_for(boost::chrono::milliseconds(interval));

    if (x == 13)
        throw std::runtime_error("Error#13");

    return x * x;
}

void may_throw(int arg)
{
    if (arg == 13)
        throw std::runtime_error("Error#13");
}

class SquareCalculator
{
    boost::promise<int> promise_;
public:
    void operator()(int x)
    {
        int interval = rand() % 5000;

        boost::this_thread::sleep_for(boost::chrono::milliseconds(interval));

        try
        {
            may_throw(x);
        }
        catch(...)
        {
           boost::exception_ptr ex = boost::current_exception();
           promise_.set_exception(ex);
           return;
        }

        promise_.set_value(x * x);
    }

    boost::unique_future<int> get_future()
    {
        return promise_.get_future();
    }
};

boost::shared_future<int> calculate_square(int x)
{
    return boost::async(boost::bind(&calculate_square, x));
}

int main()
{
    // 1st async task
    boost::packaged_task<int> pt(boost::bind(&calculate_square, 13));
    boost::unique_future<int> fresult = pt.get_future();

    boost::thread thd1(std::move(pt));

    // 2nd async task
    SquareCalculator square_calc;

    boost::unique_future<int> fsquare = square_calc.get_future();

    boost::thread thd2(boost::ref(square_calc), 13);

    // 3rd async task

    boost::unique_future<int> async_result
            = boost::async(boost::bind(&calculate_square, 25));

    // waiting for results
    while (!fresult.is_ready())
    {
        cout << "Waiting for result..." << endl;
        boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
    }

    try
    {
        cout << fresult.get() << endl;
    }
    catch(const runtime_error& e)
    {
        cout << "Exception: " << e.what() << endl;
    }

    try
    {
        cout << "Result from promise: " << fsquare.get() << endl;
    }
    catch(const runtime_error& e)
    {
        cout << "Exception from promise: " << e.what() << endl;
    }

    try
    {
        cout << "Result from async: " << async_result.get() << endl;
    }
    catch(const runtime_error& e)
    {
        cout << "Exception from async: " << e.what() << endl;
    }

    cout << "\n\nVector of futures: \n";

    vector<boost::unique_future<int>> vec_results;

    for(int i = 0; i < 20; ++i)
        vec_results.push_back(boost::async(boost::bind(&calculate_square, i)));

    for(auto& result : vec_results)
    {
        try
        {
            cout << "Result: " << result.get() << endl;
        }
        catch(const runtime_error& e)
        {
            cout << "Exception from async: " << e.what() << endl;
        }
    }

}
