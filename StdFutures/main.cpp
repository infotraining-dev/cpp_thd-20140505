#include <iostream>
#include <future>
#include <chrono>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>

namespace impl = std;

void f(std::string name)
{
    std::cout << "Start " << name << std::endl;
    impl::this_thread::sleep_for(impl::chrono::milliseconds(2000));
    std::cout << "End " << name << std::endl;
}

int main()
{
    impl::future<void> f1 = impl::async(impl::launch::async, []() { f("f"); });
    impl::future<void> f2 = impl::async(impl::launch::deferred, []() { f("g"); });

    f1.get();
    f2.get();

//    cout << "Main..." << endl;
//    this_thread::sleep_for(chrono::milliseconds(2000));

//    f1.get();
//    f2.get();

    impl::async(impl::launch::async, []() { f("f"); });
    impl::async(impl::launch::async, []() { f("g"); });

    impl::this_thread::sleep_for(impl::chrono::milliseconds(2000));
}
