#ifndef ACTIVE_OBJECT_HPP
#define ACTIVE_OBJECT_HPP

#include <boost/function.hpp>
#include <boost/thread.hpp>
#include "thread_safe_queue.hpp"

typedef boost::function<void()> Task;

class ActiveObject
{
public:
    ActiveObject() : is_done_(false)
    {
        thread_ = boost::thread(&ActiveObject::run, this);
    }

    ActiveObject(const ActiveObject&) = delete;
    ActiveObject& operator=(const ActiveObject&) = delete;

    ~ActiveObject()
    {
        send([&] { is_done_ = true; });

        thread_.join();
    }

    void send(Task task)
    {
        task_queue_.push(task);
    }

private:
    ThreadSafeQueue<Task> task_queue_;
    bool is_done_;
    boost::thread thread_;

    void run()
    {
        while(true)
        {
            Task task;

            task_queue_.wait_and_pop(task);

            task();

            if (is_done_)
                return;
        }
    }
};


#endif // ACTIVE_OBJECT_HPP
