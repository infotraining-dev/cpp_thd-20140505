#include <iostream>
#include <boost/thread.hpp>

using namespace std;

void background_work(int id, int steps)
{
    try
    {
       for(int i = 1; i <= steps; ++i)
        {
            boost::this_thread::disable_interruption ds;
            cout << "THD#" << id << ": step#" << i << endl;
            boost::this_thread::interruption_point();
            cout << "THD$" << id << ": step#" << i << endl;
            boost::this_thread::restore_interruption ri(ds);

            //boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
            boost::this_thread::interruption_point();
        }
    }
    catch(const boost::thread_interrupted& e)
    {
        cout << "Przerwano wykonanie wątku" << endl;
    }
}

int main()
{
    boost::thread thd(&background_work, 1, 10000);

    //boost::this_thread::sleep_for(boost::chrono::seconds(2));

    thd.interrupt();

    thd.join();
}
