#include <iostream>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <boost/noncopyable.hpp>

namespace nsn = boost;

using namespace nsn;
using namespace std;

void background_work(int id, size_t ms)
{
    for(int i = 0; i < 10; ++i)
    {
        cout << "THD# " << id << ": "
             << i << endl;

        nsn::this_thread::sleep_for(nsn::chrono::milliseconds(ms));
    }
}

class BackgroundTask
{
    const int id_;
public:
    BackgroundTask(int id) : id_(id)
    {}

    void operator()(size_t ms)
    {
        for(int i = 0; i < 10; ++i)
        {
            cout << "Task# " << id_ << ": "
                 << i << endl;

            nsn::this_thread::sleep_for(nsn::chrono::milliseconds(ms));
        }
    }
};

class Buffer
{
    vector<int> buffer_;
public:
    void assign(const vector<int>& source)
    {
        buffer_.assign(source.begin(), source.end());
    }

    vector<int> data() const
    {
        return buffer_;
    }
};

template <typename Container>
void print(const Container& c, const string& prefix)
{
    cout << prefix << ": ";
//    for(auto it = begin(c); it != end(c); ++it)
//        cout << *it << " ";
    for(const auto& item : c)
        cout << item << " ";
    cout << "\n";
}

void run()
{
    nsn::thread local_thd(&background_work, 101, 100);

    local_thd.detach();
}

nsn::thread async_copy(Buffer& buff, const vector<int>& data)
{
    nsn::thread local_cpy([&] { buff.assign(data);});

    return std::move(local_cpy);
}

void print_in_thread(int& x)
{
    for(int i = 0; i < 10; ++i)
    {
        cout << "Value x: " << x << endl;
        nsn::this_thread::sleep_for(nsn::chrono::milliseconds(50));
    }
}

class ScopedThread //: boost::noncopyable
{
    nsn::thread& thread_;
public:
    ScopedThread(nsn::thread& thread) :  thread_(thread)
    {}

    ScopedThread(const ScopedThread&) = delete;
    ScopedThread& operator=(const ScopedThread&) = delete;

    ~ScopedThread()
    {
        if (thread_.joinable())
            thread_.join();
    }
};

void may_throw()
{
    throw std::runtime_error("Unexpected error");
}

void local_function()
{
    int x = 13;

    nsn::thread thd(&print_in_thread, nsn::ref(x));

    ScopedThread scoped_thread(thd);

    may_throw();
}

int main() try
{
    cout << "Hardware threads: " << nsn::thread::hardware_concurrency() << endl;

    local_function();

    run();

    vector<nsn::thread> thds;
    thds.push_back(nsn::thread(&background_work, 1, 500));
    nsn::thread thd2(&background_work, 2, 250);
    thds.push_back(std::move(thd2));

    nsn::thread thd3(BackgroundTask {3}, 100);

    nsn::this_thread::sleep_for(nsn::chrono::seconds(3));

    vector<int> data = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    Buffer buff1;
    Buffer buff2;

    nsn::thread thd_cpy1(bind(&Buffer::assign, nsn::ref(buff1), nsn::cref(data)));
    nsn::thread thd_cpy2 = async_copy(buff2, data);

    for(auto& t : thds)
        t.join();

    thd3.join();

    thd_cpy1.join();

    if (thd_cpy2.joinable())
        thd_cpy2.join();

    print(buff1.data(), "buff1");
    print(buff2.data(), "buff2");
}
catch(const std::runtime_error& e)
{
    cout << "Catched: " << e.what() << endl;
}
