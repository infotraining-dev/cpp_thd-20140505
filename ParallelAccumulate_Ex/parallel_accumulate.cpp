#include <iostream>
#include <boost/thread.hpp>
#include <vector>
#include <list>
#include <algorithm>
#include <chrono>

template <typename It, typename T>
void partial_accumulate(It first, It last, T& result)
{
    result = std::accumulate(first, last, T());
}

template <typename It, typename T>
T parallel_accumulate(It first, It last, T init)
{
    unsigned hardware_threads_count = boost::thread::hardware_concurrency();

    if (hardware_threads_count == 0)
        hardware_threads_count = 1;

    std::vector<boost::thread> thds(hardware_threads_count-1);
    std::vector<T> partial_results(hardware_threads_count);

    size_t count = std::distance(first, last);
    size_t range_size = count / hardware_threads_count;

    It range_start = first;
    for(unsigned i = 0; i < hardware_threads_count - 1; ++i)
    {
        It range_end = range_start;
        std::advance(range_end, range_size);

        thds[i] = boost::thread(&partial_accumulate<It, T>, range_start, range_end,
                                boost::ref(partial_results[i]));
        range_start = range_end;
    }

    partial_accumulate(range_start, last, partial_results[hardware_threads_count-1]);

    for(auto& t : thds)
        t.join();

    return std::accumulate(partial_results.begin(), partial_results.end(), init);
}

int main(int argc, char *argv[])
{
    std::cout << "Hardware threads: "  << boost::thread::hardware_concurrency() << std::endl;
    const size_t SIZE = 5000000;
    std::vector<long> v(SIZE);
    for (size_t i = 0 ; i < SIZE; ++i)
    {
        v.push_back(i);
    }

    auto start = std::chrono::high_resolution_clock::now();
    std::cout << "Accumulate:          " << std::accumulate(v.begin(), v.end(), 0);
    auto end = std::chrono::high_resolution_clock::now();
    float seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "   " << seconds << " ms" << std::endl;

    std::vector<long> v2(SIZE);
    for (size_t i = 0 ; i < SIZE; ++i)
    {
        v2.push_back(i);
    }

    start = std::chrono::high_resolution_clock::now();
    std::cout << "Parallel Accumulate: " << parallel_accumulate(v2.begin(), v2.end(), 0);
    end = std::chrono::high_resolution_clock::now();
    seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "   " << seconds << " ms" << std::endl;
    return 0;
}
