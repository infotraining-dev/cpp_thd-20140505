#include <iostream>
#include <vector>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>

using namespace std;

class Data
{
    vector<int> data_;
    volatile bool is_ready_ = false;
    boost::mutex mtx_;
    boost::condition_variable cond_;
public:
    void read()
    {
        // ...
        data_.resize(100);
        generate(data_.begin(), data_.end(), [] { return rand() % 100; });
        boost::unique_lock<boost::mutex> lk(mtx_);
        is_ready_ = true;
        cond_.notify_all();
    }

    void process(int id)
    {
        boost::unique_lock<boost::mutex> lk(mtx_);

        cond_.wait(lk, [&]() { return is_ready_; });

        int sum = accumulate(data_.begin(), data_.end(), 0);
        cout << id << " Sum: " << sum << endl;
    }
};


int main()
{
    Data data;

    boost::thread thd1(&Data::process, &data, 1);
    boost::thread thd2(&Data::process, &data, 2);
    boost::this_thread::sleep_for(boost::chrono::seconds(1));
    boost::thread thd3(&Data::read, &data);



    thd1.join();
    thd2.join();
    thd3.join();
}
